package shlee.recommendTest;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;


import com.google.gson.annotations.SerializedName;

public class User {
	@SerializedName("id")
	private String id;

	@SerializedName("rid")
	private long rid;
	
	@SerializedName("pw")
	private String password;

	@SerializedName("nm")
	private String name;

	@SerializedName("ni")
	private String nickname;

	@SerializedName("em")
	private String email;

	@SerializedName("gn")
	private int gender;

	@SerializedName("ag")
	private int age;

	@SerializedName("th")
	private String thumbnailUrl;

	@SerializedName("pn")
	private String phoneNumber;

	@SerializedName("pt")
	private String pushToken;

	@SerializedName("tp")
	private int type;

	@SerializedName("isp")
	private boolean isProhibited;

	@SerializedName("hm")
	private int howMany;

	@SerializedName("lo")
	private boolean isLogin;

	public User() {
		super();
		id = null;
		password = null;
		name = null;
		nickname = null;
		email = null;
		gender = 0;
		age = 0;
		thumbnailUrl = null;
		phoneNumber = null;
		pushToken = null;
		type = UserType.NORMAL.ordinal();
		isProhibited = false;
		howMany = 0;
		isLogin = false;
	}

	// Because ResultSet is non-serializable
	public void serializeFromResultSet(ResultSet rs) {
		try {
			ResultSetMetaData rsmd = rs.getMetaData();

			for (int i = 1; i <= rsmd.getColumnCount(); i++) { // Column Index 1
																// ~ N
				String columnName = rsmd.getColumnName(i);
				if (columnName != null) {
					switch (rsmd.getColumnName(i)) {
					case "id":
						id = rs.getString(i);
						break;
					case "rid":
						rid = rs.getLong(i);
						break;
					case "password":
						password = rs.getString(i);
						break;
					case "name":
						name = rs.getString(i);
						break;
					case "nickname":
						nickname = rs.getString(i);
						break;
					case "email":
						email = rs.getString(i);
						break;
					case "gender":
						gender = rs.getInt(i);
						break;
					case "age":
						age = rs.getInt(i);
						break;
					case "thumbnail_url":
						thumbnailUrl = rs.getString(i);
						break;
					case "phone_number":
						phoneNumber = rs.getString(i);
						break;
					case "push_token":
						pushToken = rs.getString(i);
						break;
					case "type":
						type = rs.getInt(i);
						break;
					case "is_prohibited":
						isProhibited = rs.getBoolean(i);
						break;
					case "how_many":
						howMany = rs.getInt(i);
						break;
					case "is_login":
						isLogin = rs.getBoolean(i);
						break;
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getThumbnailUrl() {
		return thumbnailUrl;
	}

	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPushToken() {
		return pushToken;
	}

	public void setPushToken(String pushToken) {
		this.pushToken = pushToken;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public boolean isProhibited() {
		return isProhibited;
	}

	public void setProhibited(boolean isProhibited) {
		this.isProhibited = isProhibited;
	}

	public int getHowMany() {
		return howMany;
	}

	public void setHowMany(int howMany) {
		this.howMany = howMany;
	}

	public boolean isLogin() {
		return isLogin;
	}

	public void setLogin(boolean isLogin) {
		this.isLogin = isLogin;
	}

	public long getRid() {
		return rid;
	}

	public void setRid(long rid) {
		this.rid = rid;
	}
}
