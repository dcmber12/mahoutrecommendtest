package shlee.recommendTest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.common.LongPrimitiveIterator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.UserBasedRecommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;

import com.google.gson.JsonObject;


/**
 * Servlet implementation class RecommendServlet
 */
@WebServlet("/recommend")
public class RecommendServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final int RECOMMEND_SIZE = 7;
       
    public RecommendServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("Call - Recommend");
		ServletUtil.initialize(request, response);

		JsonObject responseJObj = new JsonObject();
		responseJObj.addProperty("is", false);

		JsonObject jObj = ServletUtil.getRequestJsonObject(request);
		User user = ServletUtil.getUser(jObj);
		
		long recId = user.getRid();
		if (recId < -1) {
			ServletUtil.publishOutput(response, HttpServletResponse.SC_BAD_REQUEST, responseJObj);
			return;
		}
		
		ArrayList<TocRecommend> list = new ArrayList<>();
		try {
			 list = recommend(recId);
		} catch (IOException e) {
			e.printStackTrace();
			ServletUtil.publishOutput(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, responseJObj);
			return;
		} catch (TasteException e) {
			e.printStackTrace();
			ServletUtil.publishOutput(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, responseJObj);
			return;
		}
		
		responseJObj.add("rl", ServletUtil.getTocRecListRes(list));
		responseJObj.addProperty("is", true);
		ServletUtil.publishOutput(response, HttpServletResponse.SC_OK, responseJObj);
		return;
	}

	private ArrayList<TocRecommend> recommend(long id) throws IOException, TasteException {
		
		System.out.println("recid : " + id);
		DataModel model = new FileDataModel(new File("/home/shlee/Documents/recommend.csv"));
		UserSimilarity sim = new PearsonCorrelationSimilarity(model);
		
//		DataModel model = new FileDataModel(new File("/home/shlee/Documents/test.csv"));
//		UserSimilarity sim = new PearsonCorrelationSimilarity(model);
		
		UserNeighborhood neighborhood = new ThresholdUserNeighborhood(0.1, sim, model);
		UserBasedRecommender recommender = new GenericUserBasedRecommender(model,  neighborhood,  sim);
		List<RecommendedItem> list = recommender.recommend(id, RECOMMEND_SIZE);
		
		LongPrimitiveIterator items = model.getItemIDs();
		System.out.println("item : " + items.hasNext());
		System.out.println("item id: " + items.nextLong());
		System.out.println("rl size : " + list.size());
		
		ArrayList<TocRecommend> tocList = new ArrayList<TocRecommend>();
		for (RecommendedItem item : list) {
			TocRecommend tr = new TocRecommend(item.getItemID(), item.getValue());
			tocList.add(tr);
			System.out.println("rl added : " + tr.toString());
		}
		
		return tocList;
	}

}
