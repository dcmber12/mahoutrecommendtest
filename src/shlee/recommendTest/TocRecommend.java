package shlee.recommendTest;

import com.google.gson.annotations.SerializedName;

public class TocRecommend {

	public TocRecommend(long id, float value) {
		super();
		this.id = id;
		this.value = value;
	}

	@SerializedName("id")
	private long id;
	
	@SerializedName("val")
	private float value;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public float getValue() {
		return value;
	}

	public void setValue(float value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return String.valueOf(id) + ":" + String.valueOf(value);
	}
	
	
}
