package shlee.recommendTest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

public class ServletUtil {
	
	public static void initialize(HttpServletRequest req, HttpServletResponse resp) throws UnsupportedEncodingException {

		if (req == null || resp == null)
			return;
		
		req.setCharacterEncoding("utf-8");
		resp.setContentType("application/json;charset=utf-8");
	}
	
	public static void publishOutput(HttpServletResponse response, int resCode, JsonObject responseJObj) throws IOException {
		
		if (response == null || responseJObj == null)
			return;
		
		response.setStatus(resCode);
		PrintWriter out = response.getWriter();
		System.out.println("response : " + new Gson().toJson(responseJObj));
		out.println(responseJObj);
		out.close();
	}
	
	public static JsonObject getRequestJsonObject(HttpServletRequest request) throws IOException {
		
		if (request == null)
			return null;
					
		BufferedReader reader = request.getReader();
		Gson gson = new Gson();
		JsonParser jsonParser = new JsonParser();

		if (reader == null)
			return null;
		
		JsonObject jsonObj = jsonParser.parse(reader).getAsJsonObject();
		System.out.println("request json: " + gson.toJson(jsonObj));
		return jsonObj;
	}
	
	public static User getUser(JsonObject jobj) throws IOException {
		
		if (jobj == null)
			return null;
		
		Gson gson = getGson();
		User user = gson.fromJson(jobj.get("u"), User.class);
		return user;
	}
	
	public static  User getFriend(JsonObject jobj) throws IOException {
		Gson gson = getGson();
		User friend = gson.fromJson(jobj.get("f"), User.class);
		return friend;
	}
 	
	public static  List<User> getFriendList(JsonObject jobj) throws IOException {
		Gson gson = getGson();
		Type listType = new TypeToken<List<User>>(){}.getType();
		List<User> friendList = gson.fromJson(jobj.get("fl"), listType);
		return friendList;
	}
	
	
	public static  List<String> getPhoneBook(JsonObject jobj) throws IOException {
		Gson gson = getGson();
		Type pbListType = new TypeToken<List<String>>() {}.getType();
		List<String> phoneBook = gson.fromJson(jobj.get("pb"),
				pbListType);
		return phoneBook;
	}
	
	public static  List<User> getFbFriendList(JsonObject jobj) throws IOException {
		Gson gson = getGson();
		Type fbListType = new TypeToken<List<User>>() {}.getType();
		List<User> fbFriendList = gson.fromJson(jobj.get("ffl"),
				fbListType);
		return fbFriendList;
	}
	
	public static  JsonObject getResponseJsonObject() {
		JsonObject responseJObj = new JsonObject();
		responseJObj.addProperty("is", false);
		return responseJObj;
	}
	
	public static  boolean getIsRes(int resCode) {
		return resCode == HttpServletResponse.SC_OK;
	}
	
	public static void setIs(JsonObject responseJobj, int resCode) {
		responseJobj.addProperty("is", getIsRes(resCode));
	}

	public static  JsonElement getUserRes(User user) {
		Gson gson = getGson();
		JsonParser jsonParser = new JsonParser();
		return jsonParser.parse(gson.toJson(user));
	}
	
	
	public static  JsonElement getTocRecListRes(List<TocRecommend> list) {
		Gson gson = getGson();
		JsonParser jsonParser = new JsonParser();
		return jsonParser.parse(gson.toJson(list));
	}
	
	public static  JsonElement getTocIdListRes(List<Long> list) {
		Gson gson = getGson();
		JsonParser jsonParser = new JsonParser();
		return jsonParser.parse(gson.toJson(list));
	}
	
	public static  Gson getGson() {
		return new Gson();
	}

}
